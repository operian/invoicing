class AddArticlePriceColumnToOrderLine < ActiveRecord::Migration
  def change
    add_column :order_lines, :article_price, :decimal
  end
end
