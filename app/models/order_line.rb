class OrderLine < ActiveRecord::Base
  attr_accessible :article_id, :order_id, :quantity, :article_price

  belongs_to :article
  belongs_to :order
end
